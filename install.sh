#!/usr/bin/bash

# quit if not superuser
if [ "$EUID" -ne 0 ]
    then echo "Please run as superuser or with sudo"
    exit
fi

# to-do: install needed dependencies before installing padcraft

# create resource folder in /usr/share
mkdir -p /usr/share/padcraft

# copy resources into newly created resource folder
cp -i ./padcraft /usr/share/padcraft/padcraft
cp -i ./padcraft.glade /usr/share/padcraft/padcraft.glade
cp -ir ./res /usr/share/padcraft/res
cp -i ./padcraft.desktop /usr/share/applications/padcraft.desktop

# link padcraft executable to the /usr/bin folder
if [ ! -f /usr/bin/padcraft ]; then
    ln -s /usr/share/padcraft/padcraft /usr/bin/padcraft
fi

# link launcher icon to /usr/share/applications
if [ ! -f /usr/share/applications/padcraft.desktop ]; then
    ln -s /usr/share/padcraft/padcraft.desktop /usr/share/applications/padcraft.desktop
fi

# update launcher icons
update-desktop-database

#report to user that installation is complete
echo Installation complete!
