![PadCraft](res/full_icon_256x128.png)<br />
------
A simple and lightweight digital audio workstation made with small touchscreens in mind.<br />
Designed specifically for the PinePhone, but it should work on any Linux device that supports python and GTK3.

![pads_desktop_0.png](.screenshots/pads_desktop_0.png)
![pads_mobile_0.png](.screenshots/pads_mobile_0.png)

# Dependencies
 - [Python 3](https://www.python.org/downloads/)
 - [PyGObject](https://pypi.org/project/PyGObject/)
 - [GTK 3](https://www.gtk.org/)
 - [playsound](https://pypi.org/project/playsound/)
 - [GStreamer](https://gstreamer.freedesktop.org/documentation/installing/index.html?gi-language=c) (for Linux)

# Obtaining
You can obtain PadCraft by running `git clone https://gitlab.com/nickgirga/padcraft.git` to clone the repository using git. You can also download a compressed archive ([zip](https://gitlab.com/nickgirga/padcraft/-/archive/master/padcraft-master.zip), [tar.gz](https://gitlab.com/nickgirga/padcraft/-/archive/master/padcraft-master.tar.gz), [tar.bz2](https://gitlab.com/nickgirga/padcraft/-/archive/master/padcraft-master.tar.bz2), [tar](https://gitlab.com/nickgirga/padcraft/-/archive/master/padcraft-master.tar)) using the download button in the [project overview](https://gitlab.com/nickgirga/padcraft) or from one of the releases on the [releases](https://gitlab.com/nickgirga/padcraft/-/releases) page. If you downloaded a compressed archive, decompress it using the respective tool (unzip or tar), then head to the [installation](#installation) section.

# Installation
If you aren't interested in installing PadCraft and you wish to use it as a portable application, skip to the [running](#running) section. To install PadCraft to your system and use the launcher icon, make sure you have all of the [dependencies](#dependencies) installed and run `./install.sh` or `sh ./install.sh` as superuser or with sudo. This will place the resource files in `/usr/share/padcraft`, create a link to the executable from `/usr/bin/padcraft`, and create a link to the desktop file from `/usr/share/applications/padcraft.desktop`. After the installer spits out `Installation complete!`, you should be able to launch the application using the `PadCraft` launcher icon or by running `padcraft` in a terminal.

# Removal
Run `./uninstall.sh` or `sh ./uninstall.sh` as superuser or with sudo. It will remove the resource folder at `/usr/share/padcraft`, the executable link created at `/usr/bin/padcraft`, and the desktop file link created at `/usr/share/applications/padcraft.desktop`. After the uninstaller spits out `Removal complete!`, all of PadCraft's files aside from user generated ones should be removed. Remember to uninstall any unneeded [dependencies](#dependencies), but make sure they aren't required by other software you have installed.

# Running
Make sure you have all of the [dependencies](#dependencies) installed. Simply call `./padcraft` in the root directory of the repository. If your system does not support interpreting foreign files with third-party interpreters like python using a shebang, you can run `python3 padcraft`. However this will only work if `python3` is a command that is accessible from anywhere. If your environment does not support this, add the path to your `python3` binary before it, but continue to run the command within the root folder of the repository. This script requires resources such as the glade file, so running it from any other folder will likely result in an error.

If you have installed PadCraft using the installer script, you should be able to simply launch PadCraft using the launcher icon titled `PadCraft`. You can also run PadCraft from the terminal by running `padcraft`. Note: the installer does not install [dependencies](#dependencies).

# Supported Audio Clip Formats
 - mp3
 - wav

 If your clips are in a different format, you can use free and open-source software such as [Audacity](https://www.audacityteam.org/download/) to convert them.

# Progress
What currently works? :
 - Playing audio clips with the pads
 - Changing the audio clips the pads use
