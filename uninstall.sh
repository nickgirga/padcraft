#!/usr/bin/bash

# quit if not superuser
if [ "$EUID" -ne 0 ]
    then echo "Please run as superuser or with sudo"
    exit
fi

# remove resource folder and contents in /usr/share
rm -rf /usr/share/padcraft/

# remove link to executable in the /usr/bin folder
rm -f /usr/bin/padcraft

# remove launcher icon from /usr/share/applications
rm -f /usr/share/applications/padcraft.desktop

# update launcher icons
update-desktop-database

#report to user that removal is complete
echo Removal complete!
